import {Camera} from './camera';

export const STATUSES = [
  'online',
  'offline'
];

export const URLS = [
  'https://i.picsum.photos/id/132/800/800.jpg?hmac=8fD9LqZ5JvYlvifIAipqR7OBCjAJZQulP6NHhFaHEMY',
  'https://i.picsum.photos/id/204/800/800.jpg?hmac=Ukg8ME66xbF3h87FWIWxtX_5VNn61DnubU4dsCpJNUE',
  'https://i.picsum.photos/id/322/800/800.jpg?hmac=BHML9TctQoglH91DIPkzEhtVuCIUzscYP2Xf5uUfz7w',
  'https://i.picsum.photos/id/271/800/800.jpg?hmac=fOji-jrp2udppNLJnaYSzSMs3eqrqevZtd_Ov_86Yzc',
  'https://i.picsum.photos/id/371/800/800.jpg?hmac=PD4fDPICaFVTJln3r1f8iwuUgASHubIW6XRIuJtzyEU',
  'https://i.picsum.photos/id/669/800/800.jpg?hmac=LteqK1ZD8dOyQmRTDNqdC4k6ySuo9zMjdHoh2Z2AbdU',
  'https://i.picsum.photos/id/433/800/800.jpg?hmac=RuuBZziFo3yXy-43NIfdhZU1Hdxx9gRujZfvly9GwS8',
  'https://i.picsum.photos/id/889/800/800.jpg?hmac=zjyeSx6oIhV4LLLQbX_1ZkkJygs46HUG6jBJ4-IcC6c',
  'https://i.picsum.photos/id/469/800/800.jpg?hmac=Y4ROUPBDyjwMjDzRbZCsJGRNsIvhEly7k7-fPxL35dE',
  'https://i.picsum.photos/id/49/800/800.jpg?hmac=rAzFhjqrfdnRPLR5_nFV49tMbvavk1xvsaEngwbDUfc',
  'https://i.picsum.photos/id/876/200/200.jpg?hmac=XKoZLM866KBZwrT4IBuHbiUOfu0kh_qSWT6mitQyYQo',
  'https://i.picsum.photos/id/331/200/200.jpg?hmac=otNh1Xx2hk_Tng_SFa60ayddRGORvWnDKJ2wG1l0KIE',
  'https://i.picsum.photos/id/888/200/200.jpg?hmac=k4DxIkJ_O8YKi3TA5I9xxJYJzqpSvx3QmJlgZwHMojo',
  'https://i.picsum.photos/id/10/200/200.jpg?hmac=Pal2P4G4LRZVjNnjESvYwti2SuEi-LJQqUKkQUoZq_g',
  'https://i.picsum.photos/id/604/200/200.jpg?hmac=qgFjxODI1hMBMfHo68VvLeji-zvG9y-iPYhyW0EkvOs',
  'https://i.picsum.photos/id/770/200/200.jpg?hmac=QFpEnsRd_HVWziMxXltLr7icjRJhyhL7vsBAwyXrotA',
  'https://i.picsum.photos/id/915/200/200.jpg?hmac=zZ-_EQ1TWG_LFblhB2BrD2CJYUhLEnobSCCthppN0ZE',
  'https://i.picsum.photos/id/591/200/200.jpg?hmac=5agpVWsRchY0DObXs23vYWjjgqLZEBhqSvTwfCAcyng',
  'https://i.picsum.photos/id/445/200/200.jpg?hmac=IJGybzd6hRYuiwyBiBXZ_3cOjM0MrrTpARBSFzypGNI',
  'https://i.picsum.photos/id/1023/200/200.jpg?hmac=MtNMS39i8o8sE6PiXNwABDxNtK4niBxaZWoX5KY3cyg',
  'https://i.picsum.photos/id/551/200/200.jpg?hmac=vxRitsvMJEbK15DKl4ZRj7NQWF6RTfzBvievdi9q96s',
  'https://i.picsum.photos/id/490/200/200.jpg?hmac=7WZhaN9NS8sb08YmpHre_3NGnVUsmH8X5W_GlG2Mry4'
];

export const CAMERAS: Camera[] = [
  new Camera(1, randomUrl(), randomUrl(), randomStatus()),
  new Camera(2, randomUrl(), randomUrl(), randomStatus()),
  new Camera(3, randomUrl(), randomUrl(), randomStatus()),
  new Camera(4, randomUrl(), randomUrl(), randomStatus()),
  new Camera(5, randomUrl(), randomUrl(), randomStatus()),
  new Camera(6, randomUrl(), randomUrl(), randomStatus()),
  new Camera(31, randomUrl(), randomUrl(), randomStatus()),
  new Camera(11, randomUrl(), randomUrl(), randomStatus()),
  new Camera(15, randomUrl(), randomUrl(), randomStatus()),
  new Camera(13, randomUrl(), randomUrl(), randomStatus()),
  new Camera(16, randomUrl(), randomUrl(), randomStatus()),
  new Camera(7, randomUrl(), randomUrl(), randomStatus()),
  new Camera(32, randomUrl(), randomUrl(), randomStatus()),
  new Camera(30, randomUrl(), randomUrl(), randomStatus()),
  new Camera(12, randomUrl(), randomUrl(), randomStatus()),
  new Camera(61, randomUrl(), randomUrl(), randomStatus()),
  new Camera(41, randomUrl(), randomUrl(), randomStatus())
];

export function randomNumber(min: number, max: number): number {
  return Math.floor(Math.random() * Math.floor(max));
}

export function randomStatus(): string {
  return STATUSES[randomNumber(0, STATUSES.length)];
}

export function randomUrl(): string {
  return URLS[randomNumber(0, URLS.length)];
}
