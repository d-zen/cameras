import {Injectable} from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api';
import {USERS} from '../users-mock';
import {CAMERAS, randomStatus, randomUrl} from '../cameras-mock';
import {Camera} from '../camera';

@Injectable({
  providedIn: 'root'
})

export class InMemoryDataService implements InMemoryDbService {

  createDb(): any {
    return {
      users: USERS,
      cameras: CAMERAS
    };
  }

  get(info: RequestInfo, db: {}): any {
    if (info['collection'] && info['collectionName'] === 'cameras') {
      info['collection'] = this.modifyCameras();
    }
  }

  modifyCameras(): Array<any> {
    const ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const cams = [];
    for (let i = 0; i < 10; i++) {
      cams.push(new Camera(ids[i], randomUrl(), randomUrl(), randomStatus()));
    }
    return Array.from(new Set(cams));
  }
}
