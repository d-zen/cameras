import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of} from 'rxjs';
import { Camera } from '../camera';


@Injectable({
  providedIn: 'root'
})
export class CamerasService  {
  protected baseUrl = 'api/cameras';

  constructor(
    private http: HttpClient,
  ) {
  }

  getCameras(): Observable<Camera[]> {
    return this.http.get<Camera[]>(this.baseUrl);
  }

  getCamera(id: number | string): Observable<Camera> {
    const url = `${this.baseUrl}/${id}`;
    return this.http.get<Camera>(url);
  }

  private handleError<T>(operation = 'operation', result?: T): any {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

}
