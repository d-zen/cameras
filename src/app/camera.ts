export interface Cameras {
  count: number;
  cameras: Camera[];
}

export class Camera {
  id: number;
  picturePreview: string;
  pictureLarge: string;
  status: string;

  constructor(id: number, picturePreview: string, pictureLarge: string, status: string) {
    this.id = id;
    this.picturePreview = picturePreview;
    this.pictureLarge = pictureLarge;
    this.status = status;
  }
}
