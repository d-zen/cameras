import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { throwError } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService
  ) {
  }

  canActivate(): boolean {
    return this.authService.isAuthenticated() ? true : this.logOut();
  }

  logOut(): boolean {
    this.authService.logOut().subscribe(_ => {
      this.authService.redirectToLogin();
    }, err => {
      throwError(err);
    });
    return false;
  }

}
