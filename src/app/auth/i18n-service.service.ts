import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as en from '../../assets/i18n/en.json';
import * as es from '../../assets/i18n/es.json';

export enum Languages {
  en = 'en',
  es = 'es'
}

@Injectable()
// tslint:disable-next-line:class-name
export class i18nService {
  // tslint:disable-next-line:variable-name
  private _currentLanguage = new BehaviorSubject<Languages>(null);

  constructor(
    private translate: TranslateService,
  ) {
    translate.setTranslation(Languages.en, (en as any).default);
    translate.setTranslation(Languages.es, (es as any).default);
    translate.setDefaultLang(Languages.es);
    translate.use(this.getLanguage());
  }

  use(lang: Languages): void {
    this._currentLanguage.next(lang);
    this.translate.use(lang);
  }

  getLanguage(): Languages {
    return this._currentLanguage.getValue();
  }

  get currentLanguageEvent(): BehaviorSubject<Languages> {
    return this._currentLanguage;
  }

  get currentLanguage(): Languages {
    return this._currentLanguage.getValue();
  }

  setLanguage(lang: Languages): void {
    this._currentLanguage.next(lang);
    this.translate.setDefaultLang(lang);
    this.translate.use(lang);
  }

}
