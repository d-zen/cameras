import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {Credentials, User} from './model';
import {Router} from '@angular/router';
import {HttpClient } from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  public user: User;
  protected baseUrl = 'api/users';

  constructor(
    private http: HttpClient,
    private router: Router,
  ) {
  }

  private _authentication: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get authentication(): BehaviorSubject<boolean> {
    return new BehaviorSubject<boolean>(!!this.getLocalUser()) || this._authentication;
  }

  isAuthenticated(): boolean {
    return (this.authentication && this.authentication.value) || !!localStorage.getItem('user');
  }

  login(credentials: Credentials): Observable<boolean> {
    const url = `${this.baseUrl}/${credentials.login}`;
    return this.http.get(url).pipe(
      map((user: User) => {
        if (user) {
          if (credentials.password === user.pass) {
            this.setLocalUser(user);
            this.authentication.next(!!user);
            return this.authentication.value;
          } else {
            this.authentication.next(false);
            return this.authentication.value;
          }
        }
      }, err => {
        this.authentication.next(false);
        return this.authentication.value;
      }),
      catchError(this.handleError<User>('getUser', null)));
  }

  logOut(): Observable<boolean> {
    this.clearLocalUser();
    this.authentication.next(false);
    return this.authentication;
  }

  public getLocalUser(): string {
    return localStorage.getItem('user');
  }

  public clearLocalUser(): void {
    localStorage.setItem('user', '');
  }

  public setLocalUser(user: User): void {
    localStorage.setItem('user', user.login);
  }

  redirectToLogin(): void {
    this.router.navigate(['/login'], {replaceUrl: true});
  }

  private handleError<T>(operation = 'operation', result?: T): any {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

}
