import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../auth.service';
import {User} from '../../model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup = this.formBuilder.group({
    login: ['', Validators.compose([Validators.required])],
    password: ['', Validators.compose([Validators.required])],
  });
  submitted: boolean;
  wrongCredentials: boolean;
  loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {
  }

  ngOnInit(): void {
    this.authService.clearLocalUser();
  }

  login(): void {
    this.authService.login(this.loginForm.value).subscribe((user: any) => {
      if (user) {
        this.router.navigate(['/home']);
      } else {
        this.wrongCredentials = true;
      }
    }, err => {
      this.wrongCredentials = true;
    });
  }
}
