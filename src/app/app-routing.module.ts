import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './auth/components/login/login.component';
import {AuthGuard} from './auth/auth.guard';
import {HomeComponent} from './pages/home/home.component';
import {CameraComponent} from './pages/camera/camera.component';
import {SettingsComponent} from './pages/settings/settings.component';
import {ReportingComponent} from './pages/reporting/reporting.component';

const routes: Routes = [
  {
    path: ':language',
    // path: '',
    pathMatch: 'prefix',
    children: [
      {
        path: '', redirectTo: 'login', pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'settings',
        component: SettingsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'reporting',
        component: ReportingComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'home',
        component: HomeComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'home/:id',
        component: CameraComponent,
        canActivate: [AuthGuard]
      },

      {
        path: '**',
        redirectTo: 'login',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
