import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {throwError} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {
  @Input() hideBar: boolean;

  constructor(
    public authService: AuthService,
    protected router: Router
  ) { }

  ngOnInit(): void {
  }

  logOut(): void {
    this.authService.logOut().subscribe(_ => {
      this.authService.redirectToLogin();
    }, err => {
      throwError(err);
    });
  }

  active(link: string): boolean {
    return this.router.url.includes(link);
  }


}
