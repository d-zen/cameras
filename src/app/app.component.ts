import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, NavigationStart, Router} from '@angular/router';
import {AuthService} from './auth/auth.service';
import {Subscription} from 'rxjs';
import {filter} from 'rxjs/operators';
import {i18nService, Languages} from './auth/i18n-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy{
  noShell = ['/login', '/registration'];
  title = 'cameras-app';
  hideShell = true;
  private subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private i18n: i18nService,
    private authService: AuthService,
  ) {
      this.subscriptions.push(router.events.pipe(
        filter(event => {
          return event instanceof NavigationStart || event instanceof NavigationEnd;
        })
      ).subscribe((event: NavigationStart | NavigationEnd) => {
        if (event instanceof NavigationStart) {
          const lang = this.hasLanguageParams(event.url);
          if (lang) {
            if (lang !== this.i18n.currentLanguage) {
              this.i18n.use(lang);
            }
          } else {
            this.i18n.use(Languages.en);
            let url = event.url;
            if (url === '/') {
              url = this.authService.isAuthenticated() ? '/home' : '/login';
            }
            this.router.navigateByUrl(`/${this.i18n.currentLanguage}${url}`);
          }
        } else {
          this.checkShellCondition(event.urlAfterRedirects ? event.urlAfterRedirects : event.url);
          window.scrollTo(0, 0);
        }
      }));
    }

  ngOnDestroy(): void {
    if (this.subscriptions.length) {
      this.subscriptions.forEach(sub => sub.unsubscribe());
    }
  }

  ngOnInit(): void {
  }


  checkShellCondition(url: string): void {
    this.hideShell = false;
    this.noShell.forEach(nsUrl => {
      if (url.indexOf(nsUrl) > -1) {
        this.hideShell = true;
      }
    });
  }

  hasLanguageParams(url: string): Languages {
    if (!url) {
      return null;
    }
    const key = Object.keys(Languages).find(lang => url.split('/').indexOf(Languages[lang]) > -1);
    return key ? Languages[key] : null;
  }
}
