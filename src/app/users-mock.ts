import { User } from './auth/model';

export const USERS: User[] = [
  { id: 'test', login: 'test', name: 'test', pass: '9qW1Cm70AS', status: 'online' },
  { id: 'admin', login: 'admin', name: 'admin', pass: 'admin', status: 'online' },
  { id: 'user', login: 'user', name: 'user', pass: 'user', status: 'online' },
];

