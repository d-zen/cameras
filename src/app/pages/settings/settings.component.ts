import { Component, OnInit } from '@angular/core';
import {i18nService, Languages} from '../../auth/i18n-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  public Languages = Languages;
  public selectedLanguage: Languages;

  constructor(
    private i18n: i18nService,
    private router: Router
  ) {
    i18n.currentLanguageEvent.subscribe((lang: Languages) => {
      this.selectedLanguage = lang;
    });

  }

  ngOnInit(): void {
  }

  changeLanguage(language: Languages): void {
    const url = this.router.url.replace(`/${this.selectedLanguage}/`, `/${language}/`);
    this.i18n.setLanguage(language);
    this.router.navigateByUrl(url).then(() => {});
  }

}
