import { Component, OnInit } from '@angular/core';
import { CamerasService } from '../../services/cameras.service';
import { ActivatedRoute } from '@angular/router';
import { Camera } from '../../camera';

@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.scss']
})
export class CameraComponent implements OnInit {
  public camera: Camera;
  public id: number;

  constructor(
    private camerasService: CamerasService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.getCamera();
  }

  getCamera(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.camerasService.getCamera(this.id).subscribe(cam => this.camera = cam);
  }
}
