import {Component, OnDestroy, OnInit} from '@angular/core';
import {CamerasService} from '../../services/cameras.service';
import {Camera} from '../../camera';
import {Router} from '@angular/router';
import {Subscription, timer} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  cameras: Camera[];
  loader: boolean;

  constructor(
    private camerasService: CamerasService,
    protected router: Router
  ) {
  }

  ngOnInit(): void {
    this.subscription = timer(0, 1000).pipe(map(() => {
      this.getCameras();
    })).subscribe(_ => {});
  }


  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }


  getCameras(): void {
    this.loader = true;
    this.camerasService.getCameras().subscribe(resp => {
        this.loader = false;
        this.cameras = resp;
        }, err => {
        // console.error(err);
        this.loader = false;
      }
    );
  }

  navigate(id: number | string): void {
    const url = `home/${id}`;
    this.router.navigateByUrl(url);
  }
}
